import UIKit
import SnapKit

protocol VenueCardsViewDelegate: class {

    func venueDidScroll(venue: Venue)

}

class VenueCardsView: UIScrollView {

    weak var cardsDelegate: VenueCardsViewDelegate?

    private var venues: [Venue] = []

    init() {
        super.init(frame: .zero)

        self.delegate = self
        isPagingEnabled = true
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func scrollToVenue(venue: Venue) {
        var venueIndex = 0
        for (i, cardVenue) in venues.enumerated() {
            if venue.id == cardVenue.id {
                venueIndex = i
            }
        }

        setContentOffset(CGPoint(x: self.frame.width * CGFloat(venueIndex), y: 0.0), animated: true)
    }

    func setVenues(venues: [Venue]) {
        self.venues = venues
        layoutIfNeeded()
        setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
        contentSize.width = CGFloat(self.frame.width * CGFloat(venues.count))
        for view in subviews {
            view.removeFromSuperview()
        }

        for (i, venue) in venues.enumerated() {
            let venueView = VenueView(venue: venue)
            venueView.layer.cornerRadius = 16.0
            venueView.layer.shadowColor = UIColor.black.cgColor
            venueView.layer.shadowOpacity = 1.0 / 3.0
            venueView.layer.shadowOffset = .zero
            venueView.layer.shadowRadius = 8

            let containerView = UIView()
            containerView.addSubview(venueView)

            addSubview(containerView)

            containerView.snp.makeConstraints { make in
                make.left.equalTo(self).offset(self.frame.width * CGFloat(i))
                make.height.equalTo(self).multipliedBy(2)
                make.width.top.equalTo(self)
            }

            venueView.snp.makeConstraints { make in
                make.bottom.equalTo(containerView)
                make.top.left.equalTo(containerView).offset(16)
                make.right.equalTo(containerView).offset(-16)
            }
        }
    }

}

extension VenueCardsView: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollingFinished(scrollView: scrollView)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            scrollingFinished(scrollView: scrollView)
        }
    }

    func scrollingFinished(scrollView: UIScrollView) {
        let index = Int(round(scrollView.contentOffset.x / self.frame.width))
        guard venues.count >= index - 1 else {
            return
        }

        cardsDelegate?.venueDidScroll(venue: venues[index])
    }

}
