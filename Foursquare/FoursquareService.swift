import Foundation
import MapKit

class FoursquareService {

    private let clientID: String
    private let clientSecret: String
    private let session: URLSession

    private let baseURL = "https://api.foursquare.com/v2/"

    init(clientID: String, clientSecret: String) {
        self.clientID = clientID
        self.clientSecret = clientSecret
        self.session = URLSession.shared
    }

    func search(latitude: Double, longitude: Double, radius: Int, completion: ((SearchResponse?) -> ())? = nil) {
        makeRequest(endpoint: "/venues/search", params: [
            "ll": String(describing: latitude) + "," + String(describing: longitude),
            "radius": String(describing: radius),
            "intent": "browse",
            "limit": "10",
        ], completion: completion)
    }

    private func makeRequest<T: Codable>(endpoint: String, params: [String: String] = [:], completion: ((T?) -> ())? = nil) {
        let url = URL(string: baseURL + endpoint + makeUrlParams(params: params))!
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data else {
                completion?(nil)
                return
            }

            let result = try? JSONDecoder().decode(FoursquareResponse<T>.self, from: data)
            completion?(result?.response)
        }

        task.resume()
    }

    private func makeUrlParams(params: [String: String]) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"

        var requestParams = params
        requestParams["client_id"] = clientID
        requestParams["client_secret"] = clientSecret
        requestParams["v"] = formatter.string(from: Date())

        var firstParam = true
        var urlParams = ""
        for (key, value) in requestParams {
            if firstParam {
                urlParams += "?"
                firstParam = false
            } else {
                urlParams += "&"
            }

            urlParams += key + "=" + value
        }

        return urlParams
    }

}
