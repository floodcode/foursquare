import UIKit
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let initLocation = CLLocation(latitude: 50.468471, longitude: 30.514746)
    let initRadius = CLLocationDistance(500)

    func applicationDidFinishLaunching(_ application: UIApplication) {
        let rootController = MapController(container: Container(), location: initLocation, radius: initRadius)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootController
        window?.makeKeyAndVisible()
    }

}
