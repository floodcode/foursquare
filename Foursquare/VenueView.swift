import UIKit

class VenueView: UIView {

    init(venue: Venue) {
        super.init(frame: .zero)

        backgroundColor = .white

        let cardLabel = UILabel()
        cardLabel.text = venue.name
        cardLabel.font = .systemFont(ofSize: 24)
        addSubview(cardLabel)

        cardLabel.snp.makeConstraints { make in
            make.left.top.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
