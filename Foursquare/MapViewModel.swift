import Foundation
import RxSwift

class MapViewModel {

    let venuesSubject = PublishSubject<[Venue]>()

    let container: Container

    init(container: Container) {
        self.container = container
    }

    func locationChanged(latitude: Double, longitude: Double, radius: Int) {
        container.foursquareService.search(latitude: latitude, longitude: longitude, radius: radius) { [weak self] response in
            guard let response = response else {
                return
            }

            DispatchQueue.main.async {
                self?.venuesSubject.onNext(response.venues)
            }
        }
    }

}
