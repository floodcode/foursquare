import Foundation

struct FoursquareResponse<T: Codable>: Codable {

    let meta: FoursquareMeta
    let response: T

}

struct FoursquareMeta: Codable {

    let code: Int
    let requestId: String

}

struct SearchResponse: Codable {

    let venues: [Venue]

}

struct Venue: Codable {

    let id: String
    let name: String
    let location: VenueLocation

}

struct VenueLocation: Codable {

    let address: String?
    let lat: Double
    let lng: Double

}
