import UIKit
import MapKit
import SnapKit
import RxSwift

class VenueAnnotation: NSObject, MKAnnotation {

    let venue: Venue

    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: venue.location.lat, longitude: venue.location.lng)
    }

    init(venue: Venue) {
        self.venue = venue
    }

}

class MapController: UIViewController {

    // Views

    private var mapView: MKMapView!
    private var cardsView: VenueCardsView!

    // View model

    private let viewModel: MapViewModel

    // Properties

    private let container: Container
    private let initLocation: CLLocation
    private let initRadius: CLLocationDistance

    private let disposeBag = DisposeBag()

    init(container: Container, location: CLLocation, radius: CLLocationDistance) {
        self.container = container
        self.initLocation = location
        self.initRadius = radius

        self.viewModel = MapViewModel(container: container)

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up map view

        mapView = MKMapView()
        mapView.delegate = self
        view.addSubview(mapView)

        mapView.snp.makeConstraints { make in
            make.top.bottom.left.right.equalTo(self.view)
        }

        mapView.centerMapOnLocation(location: initLocation, radius: initRadius)

        viewModel.venuesSubject.subscribe(onNext: { [weak self] (venues: [Venue]) in
            guard let self = self else {
                return
            }

            var venues = venues
            venues.sort { (x, y) -> Bool in
                return x.location.lng < y.location.lng
            }

            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotations(venues.map { venue -> MKAnnotation in
                return VenueAnnotation(venue: venue)
            })

            self.cardsView.setVenues(venues: venues)
        }).disposed(by: disposeBag)

        // Set up venue cards

        cardsView = VenueCardsView()
        cardsView.cardsDelegate = self
        view.addSubview(cardsView)

        cardsView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view.snp.bottom).offset(-256 + 64)
        }

        let latitude = initLocation.coordinate.latitude
        let longitude = initLocation.coordinate.longitude
        let radius = Int(initRadius)
        viewModel.locationChanged(latitude: latitude, longitude: longitude, radius: radius)
    }

}

// MARK: - MKMapViewDelegate

extension MapController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? VenueAnnotation else {
            return
        }

        mapView.setCenter(annotation.coordinate, animated: true)
        cardsView.scrollToVenue(venue: annotation.venue)
    }

}

// MARK: - VenueCardsViewDelegate

extension MapController: VenueCardsViewDelegate {

    func venueDidScroll(venue: Venue) {
        mapView.annotations.forEach { annotation in
            guard let annotation = annotation as? VenueAnnotation else {
                return
            }

            if annotation.venue.id == venue.id {
                mapView.setCenter(annotation.coordinate, animated: true)
                mapView.selectAnnotation(annotation, animated: true)
                return
            }
        }
    }

}
